# aero-gatsby-base


Base module that encapsulates common functionality, components, configuration, and best-practices, for Aero Agency Gatsby sites.
